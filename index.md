# Mental change for sceptics and analytical people

This document covers the basic ideas for how to create mental change, to turn
your life around or simply to make some things easier, even though all the
magical tricks you've heard of simply won't work for you.

## Problem.

So, your life isn't going quite according to plan? Welcome to the club! Don't
worry, you don't have to wear a name tag, sit in a circle of strangers and
recount your life's story.

What matters is that, as a member of the target audience of this document
(based on no market research whatsoever), you know that many, many problems
that have a big psychological component are "all in your mind", and in theory
that is quite clear to you but it doesn't seem like you can really cash in on
that. The implied "... so it must be easy to fix" seems like it works
perfectly for some people... but not for you. You're almost inclined to think
that maybe these people simply have way smaller problems than you, or are just
lying through their teeth about how effective their chosen magic method is.
And, really, magic doesn't exist, so how could what they say be true, right?

To name a few things that might qualify as magic methods: meditation,
autogenic training, self-hypnosis, positive affirmations, the law of
attraction, "It Works", Sedona Method, EFT, NLP, EMDR and a bunch of other
acronyms and fancy names. I'm not going to pass judgement on all of them,
because as you'll see, I have a rather broad view on everything in this
general direction.

Evaluating any of these you'll see a whole bunch of die-hard fans and die-hard
critics. How come? If you're kind of equal parts hopeful and sceptical about
these types of magic methods, that looks a bit foreboding, doesn't it? Like
you're maybe going to have to try them all and see which one will work for
you, because none of them work for everyone. I'm going to argue how that is
kind of true but not really, with the goal of making it not matter a lot what
exactly you end up doing, as long as you go by a few simple rules.

Looking at the popular narrative of each of these magic methods, at least
according to their own marketing departments, it typically sounds like your
problem is trivial to solve, but at this point that wouldn't be doing your
quest justice. You've probably already tried a bunch of things, some of them
bordering on esoteric, and yet none of them have really clicked for you. You
may even have gone through a few days of almost boundless excitement and
enthusiasm, got some (very) temporary astounding results, but in the end you
were back where you started. Perhaps you didn't even get that much out of your
trials.

So what now? Well, allow me to drill down to the common themes of most magical
solutions to problems in your mind, before we can get down to troubleshooting
them.

Side note: I'm not going to "fix scepticism" by providing incontrovertible
proof that everything I say is undisputable truth. No such proof exists.
Instead, I'm going to take your general willingness to accept that these
methods *can* work, and hand you some more or less sound reasoning on how you
can use insight gained from them.

## Solution? You wish!

Virtually all of the solutions that profess to be super effective and fast are
based on focusing on some kind of idea or concept, to suffuse your mind in
something that helps shake loose the things that seem to be stuck in your
thoughts and feelings, and bring in sunshine and unicorns. Or something like
that.

Sometimes you have a stronger element of *not* focusing on things, but rather
learning how to wrench your focus away from some elements of your experience,
so there the idea is that you basically "just stop" doing the things you don't
want to be doing (though they typically try to make it a little more doable
than that).

> Few of the magic methods for mental change get to the core of what drives
> learning; most focus on rituals.

I'm going to focus on what I've found to be the least mysticized form of
deliberate change of stuck mental patterns... though if you read up on it,
you'll probably get a very different idea. My choice is self-hypnosis.

There's a lot of mumbo-jumbo people associate with the word "hypnosis" but the
basic idea is very simple: relax your mind so that it's easier to just take
new things on board. Take new things on board. Done!

Now of course that's much easier said than done for many people... so I'm
going to step away from that a little and focus less on relaxing and more on
ways to take things on board, period. Relaxation is kind of convenient for
that because it's nice and comfortable to feel good while changing your mind,
so we'll be picking that up along the way.

The general process clearly works for at least some people, and if you've
looked into self-hypnosis you've probably seen enthusiastic responses from
those who have been super successful with it. You, less so. If you've kept at
it a little, perhaps you've managed to feel kind of somewhat more relaxed in
your body, or parts of it, and everything else was kind of
I-dunno-I-guess-maybe-it's-working-a-little, and it never really went any
further than that. And what the heck is mental relaxation supposed to be,
anyway?

People tend to think of hypnosis as this extremely unreal state of
consciousness in which you act like a zombie and your mind is... "gone". While
that's something that's *possible* to experience, it's not really a definite
part of hypnosis, nor of this mysterious "mental relaxation".

In fact, a closely related term, "trance", is just as much not extremely
spectacular. One thing that routinely trips up people when trying
self-hypnosis is that they expect to experience something unreal or magical,
and then that doesn't happen. Failure, right? Well, no... just not what you
expected, but maybe still useful.

Anyway. Let's put aside magical states, and anything that reads like one, and
focus on what you can actually *do*.

## Back to the drawing board

If you've tried to figure out why it doesn't work for you, there are some
ideas you've probably come across:

* It's wishful thinking and doesn't actually really change anything.
* It doesn't work if you're analytically-minded or "too smart".
* It only works on weak-willed folks.
* I just can't get into the right mental state.
* It can't create permanent change.

All of these are missing the point, and to help you get an idea why I'm saying
that, I'm going to share my model of mental change that I've been developing
for about a decade now. I'll even spare you the boring details of my epic
quest thing because I'm a nice guy.

### The ultra-condensed primer on learning

Between the moment you were born and now, you learned an absolutely astounding
amalgamation of things. Ever had to learn a foreign language in school? Seemed
like there were a lot of things you had to work on, an almost insurmountable
task, right? Well, you learned your own language before that, and all without
the benefit of a carefully designed teaching method... and it never felt
overwhelming, did it?

Looking back on it, can you remember it feeling difficult in *any* way? No you
can't, because your mind is really good at this stuff. In fact, before you're
confronted with reasons against learning (such as the classics, "I don't want
to" and "you're terrible at this and will never improve"), learning just
happens, seemingly all on its own.

What does the mind do? It discovers patterns. It finds ways to group some
things together, distinguish other things, and it keeps on finding patterns in
patterns until it's got an internal representation of something that is
sophisticated enough to be useful in some way.

For instance, if you start learning to play an instrument, at the beginning
you may have trouble even just putting your fingers in roughly the right
places... but a few months later (provided you do your learning more or less
effectively), these things happen completely automatically. Your mind has
learned, and you no longer need to trouble yourself with the details.

There are some important factors for this type of learning:

* There needs to be some level of **variation**. While it's perfectly possible
  to learn with just one example, it's also risky. For example, phobias are an
  example of one-shot learning: you learn, in a single event, to associate
  extreme levels of fear with something like a dog. This is an example
  learning that's not *helpful*, even though it's certainly very effective. If
  there's more variation, your mind can delineate better... for example, it
  can discover that some dogs are harmless, some are a bit overeager, and some
  are kind of better to stay away from.
* You need **feedback**, ideally as quickly as possible. When you *do*
  something, or make guesses, if you never get any response to it there's
  really no way to know whether you did well. Feedback can be verbal or
  non-verbal communication, or just a result (for instance, if you throw a
  flat stone the right way, the feedback is that it will skip across the
  water). If there is no relevant feedback at all, or very delayed feedback,
  learning becomes impossible, or very hard.
* Things you pay **attention** to have the potential to factor more strongly
  into learning.
* Things you **respond strongly** to, mentally, are learned "more strongly".

With many, many learnable things, you can use these factors strategically.
For example, knowledge- and fact-based learning relies on *understanding* a
lot: you have to "make sense" of information. What this really means is that
you find ways to fit this information into all kinds of contexts. Knowing that
addition combines two numbers from a scale into another number in a certain
way is one thing... *using* addition in real life is what eventually allows
you to do it very quickly, almost without having to think about it if you use
it often enough, but you don't get there without understanding the principle
first. So, strategic approaches include the following:

* Purposefully varying the things you look at during learning, not too
  slightly but also not too extremely,
* finding ways to get good and quick feedback on your own attempts (based on a
  theoretical framework, with the help of a teacher, or just by finding the
  right context to practice in),
* paying attention to what's going on, and
* being keenly interested in, or feeling good about, what you're learning (or
  how you're learning, or basically anything related to the learning you're
  doing).

All these will almost certainly make learning much, much easier.

Takeaway:

> To learn something, pay attention, feel good/excited/interested, find ways
> to get good and quick feedback, and add some variation.

Eventually, the result of learning is that you can do something without
thinking about it. A master of playing the guitar can hold a conversation
while playing a not-too-devilishly-tricky piece, for example. The mind has
created stable associations between various actions and the instant feedback
the guitarist gets from the feeling of the strings and frets on their fingers,
the sounds, etc. The piece itself is memorized similarly. All of it needs no
conscious input at this stage.

This is great for things you wanted to learn, but not so great for things you
*didn't* want to learn, such as that phobia I mentioned or, for instance,
having a hard time achieving goals you set for yourself. As it happens,
strong negative mental responses intensify learning just as much as positive
responses... which is also why phobias can happen.

### The ultra-condensed primer on unlearning

This is where it gets interesting. If we go by how learning works, we can
quickly conclude that you just have to vary your inputs and get the right
feedback and that's it -- your phobia melts like snow in June. Except you know
that doesn't work.

Let's try anyway, with a semi-semi-practical example. Suppose you have this
problem that you get disproportionately annoyed when someone uses "you're" or
"your" incorrectly. When trying to unlearn that, could we create variation?
Sure, I could just pick a properly written source and a poorly written one,
and switch back and forth. How about feedback? That's difficult. If your text
contains an incorrect word, that feedback is inside you're own mind, and the
pattern is already established (if you actually suffer from this problem, I
just gave you a free example in that sentence). For unlearning the pattern you
would have to temporarily switch it off, otherwise you won't get the feedback
you want your mind to internalize. Catch-22!

The problem, in short:

> When you try to unlearn something, the feedback pattern inside your mind
> conflicts with the result you want.

So, we have to somehow sidestep this. That's where all the magic methods come
in, of course. The basic premise is that they allow you to bypass existing
patterns, if only temporarily, to create new ones. Here are some recurring
themes from the magic methods I listed above:

* **Not interfering.** By not engaging with the feedback, you stop the mind
  from turning the feedback into a new layer of learning. Given the underlying
  idea that learnings don't stick around forever if you don't reinforce them
  periodically, this allows you to gradually get rid of a pattern, or at least
  shake it loose, and so creating a new one becomes easy.
* **Focusing on something else**, a.k.a. distraction. Again, this
  de-emphasizes the impact of the feedback from your existing mental pattern.
* **Manipulating the mental response** to loosen up that end of the pattern,
  so the mind handles it differently than before. This can create very quick
  change but takes some finesse to pull off.
* **Changing the context**. Learning is context-sensitive: it's quite easy to
  learn that as a driver, at a red light you slam the brake and as a
  pedestrian, you stop walking instead. It's pretty much impossible that you'd
  accidentally get the two mixed up (and thank goodness for that). If you
  manage to alter the context, the preconditions of a mental pattern, enough
  that the mind isn't sure that the pattern applies, you get an opportunity to
  wedge in a new mental response from the side.

I could go into a lot of detail here but this is an ultra-condensed primer, so
a few basic ideas will have to be enough for now.

All of the magic methods I listed earlier as examples are based on at least
one of these themes. Our goal here is to use as many of them as it takes to
get results.

## Let's get to it: making simple changes

So, we've looked into what the mind does and what its limitations are for
changing what's already in there, and basic ideas on how to work around that.
Let's get to the practical applications. (In the self-help industry, this is
where the sales pitch ends and where you're offered a rich selection of
products to either make everything happen for you or give you the closely
guarded secrets of underground cults. I just like to share, so let's keep
going... though I may follow up with something more in depth at *very
attractive prices!* You know, monetization and all that.)

As I said, I'm going to use the basic structure of self-hypnosis:

1. "Relax the mind" (change the context, focus on something else, ...)
2. Learn new patterns
3. ???
4. Profit!

As I said, don't get too hung up on the idea of relaxing the mind. It's just
one way to make this happen.

We're going to mix in all the basic ideas extracted from the various magic
methods. For the heck of it, I'm going to reuse my "your"/"you're" example.
Our fictitious goal is to stop feeling so annoyed and just read on.

It's difficult to "stop feeling so annoyed" without something to replace
that, by the way. The mind doesn't just do a full stop... and if we don't come
up with some general idea of what type of replacement pattern we want,
something more or less random will happen. Let's not take the chance. "Just
read on" is simple and neutral... but possibly too neutral for it to be able
to compete with the existing pattern. "Neutral" is pretty close to "no
replacement". You may have to replace the "feeling annoyed" with something
more innocuous first, something that is easier to shrug off and let fade away
in time.

"Feel all warm and tingly inside" would work, but that might be a bit too much
if you often read things on the internet. You might even end up reading *more*
poorly written things to get that warm and tingly feeling. Clearly that's not
what we want to achieve here.

There are a number of ways to create sufficiently strong responses that won't
get in the way. One of my favourites is a sense of achievement: feel good, but
*not* about the word itself -- instead, feel good about your ability to
change. On the surface, the goal state doesn't change; you still want to
achieve a good feeling when you notice a wrong word, but when that good
feeling comes, you simply decide you're feeling good about your skill at
changing things in your mind, not about the word itself.

With that in mind, let's look at specific ideas on how to actually give that
kind of change a chance to happen:

* Not interfering: when you do get your old mental pattern while trying to
  learn a new response, remind yourself that this is normal and happens
  sometimes. We're all human and everything doesn't always go perfectly. And
  it doesn't need to! Even if the pattern comes up a lot at first, by
  tinkering with all the other parameters you'll flush it out eventually and
  the less you get distracted by still getting the pattern for a while, the
  easier it is. So, it's okay to still get very annoyed at first while trying
  to get rid of that annoyance, that's part of the process and if you just let
  it happen and focus more on finetuning your approach to change, it's no big
  deal.
* Focusing on something else: for instance, perhaps you can create some other
  response *at the same time* you're getting the established pattern. Perhaps
  you can get annoyed while, at the same time, making fun of your getting
  annoyed... that will be sure to throw some kind of wrench in the pattern.
  You can even use some fringe logic: when the old pattern comes up,
  capitalize on your mental awareness of what's going on, and feel good about
  *that*. Pat yourself on the back (mentally), just for the small detail of
  being fully aware of your reaction to reading that word, and while also
  being aware that you're aiming to change it and are currently doing
  some ninja thinking to do that. Thinking in this kind of twisted way may
  seem strange or silly at first... but you'll get used to it, and then it
  will work.
* Manipulating the association: this takes a bit of practice and
  experimentation. Perhaps if you get a "yor" or a "yar" in the place of a
  misused word, that's not something you'll get annoyed about. Then you can
  capitalize on your mental reaction of... nothing special, really... and
  slowly morph the "yor"/"yar" into the wrong word that would previously have
  triggered your feeling annoyed, over a series of trials/experiments. Since
  you've prepared a context of neutrality and slowly blend it into the old
  context, there's a real chance that that reaction won't come up as it used
  to.
* Manipulating the association, take 2: it may be hard to consciously react in
  a less annoyed way, but can you purposely react *more* annoyed? Can you
  really overdo it, to the extent of throwing a major mental tantrum in your
  head? Mentally smash imaginary chairs against imaginary walls? If you can,
  you've manipulated the association, and you can keep changing it until it's
  a caricature of the previous annoyance, or something else entirely. After a
  while of that, see what it does to the original pattern... It's important
  that if you do this, you keep going and ham it up until the annoyance
  "implodes" and changes into something completely different, otherwise you
  might just end up reinforcing your annoyance. Alternatively, adding a sense
  of silliness to the overacted annoyance might do the trick, too.
* Manipulating the assocation, take 3: based on the idea of submodalities from
  NLP, alter a quality of how the annoyance "works"/feels for you. Do you have a
  mental voice that represents the annoyance? Make it sound boring or silly or
  sexy. Do you construct mental imagery (vividly or abstractly, doesn't
  matter?) Turn it blurry, black-and-white, small, grainy, confusing, anything
  that is associated with wear and tear, roughly speaking (or whatever else
  represents "this idea is on its way out" to you). The same kind of mutating
  can be done with any of the other primary senses: taste, smell, body sense,
  etc. -- though my guess is that most people don't generally process
  annoyance through their sense of smell.
* Changing the context: for instance, turn it into a game of finding as many
  wrong words in a text as quickly as possible. Not only will this change the
  "meaning" of coming across an incorrect word (it's now a "good thing") but
  it also won't give you that much time to get properly worked up. Or think of
  it as making a "pity tally" for the author, a way to create a "worst of"
  list. Or take finding an incorrect word as a prompt to come up with an
  alternative sentence that uses that incorrect word appropriately but gets
  about the same meaning across (thus training your language skills, too!).

The limit on all of these is your creativity. The specifics of what you do can
be as silly as they need to be (in my experience, ridiculous things are much
easier to come up with when you first get into brainstorming something), as
long as you stick with the spirit of the basic ideas.

> All of this is not particularly difficult to use for small changes and
> anything you haven't tried too hard to change so far.

### ... or less simple changes

When you've had enough time to really cement in an existing pattern by trying
to fight against it in the traditional way (and strongly feeling really bad
about the existing pattern which helps drive it in further), it's a different
story. The same principles will still work, but generally speaking every
approach you've tried in the past will be difficult to get to work now,
because your previous experiences will factor into your mental patterns, plus
you've had a lot of time to generalize them -- they'll tend to happen more
easily in more situations (and if that's not actually happening, you're lucky
in a sense, because it will give you more wiggle room for changing things up).

* The stronger a pattern is, the harder it becomes to not interfere, to be
  equanimous.
* The stronger a pattern is, the more it will suck you in and make it
  difficult to focus on anything else.
* The stronger/more general a pattern is, the more you'll have to manipulate
  the association to reach the boundaries of the pattern.
* The more general a pattern is, the more you'll have to change its context so
  it no longer "goes off".

So, if you don't have a lot of practice with the first two approaches (letting
go and distracting yourself), they won't help you. (More on how to build up
those skills later.)

The other two will still work, but you'll have to be rather more
careful/resourceful:

* Manipulating the association "honestly" becomes very difficult if there's a
  lot of emotional baggage attached to it. What do I mean by that? Well,
  in films you'll sometimes see characters being really scared but trying to
  act brave: "h-hello? anyone there? I have to w-warn you, I've got a g-gun!"
  The braveness is not genuine. If you manipulate the assocation into
  something that isn't genuine, it won't make a real difference. On the other
  hand, the example I had above -- deliberately overdoing the response --
  well, some responses are so powerful that it seems impossible to overdo
  them.

* Changing the context will tend to take some creativity. For example, suppose
  you want to get over some terrible loss you've experienced. The pattern here
  is that something reminds you of whatever you've lost and the mental
  response is all the negative feelings attached to that loss, and probably
  wanting the past to be different. It's not so easy to come up with a way to
  shift that into something different in a way that feels genuine, is it?

With any very imposing mental pattern, if you're having to try and fix it on
your own, without a neutral person to help you who has a fair bit of practical
experience with these strategies, honestly your chances of attacking the issue
as I described above aren't too excellent. At that point the next best thing
to do is making the change on a symbolical/indirect level. The idea here is
that the mind can pick up new patterns from almost anything. I've experienced
a few profound changes from reading, well, fiction.

The problem is that as someone who is predisposed to being sceptical, it's
difficult to cope with, to put it plainly, nonsense and genuinely accept the
idea that it will make a positive difference for you, despite all the evidence
that that is, in fact, the way the human mind works. Tricking yourself
couldn't possibly work, right? Well, that's precisely what this whole article
is about: teaching yourself to trick yourself for fun and profit.

## Bottom-up problem solving

In many analytical fields there's the idea of top-down vs. bottom-up
approaches to something. Top-down, a.k.a. "divide and conquer", means you take
a big and complicated thing and split it up into smaller things. Each of these
smaller things, you can split up again. Rinse and repeat until each part is
manageable, and eventually your big thing is covered completely.

This is the way people often try to understand something that eludes
understanding. Unfortunately, with some things, including those that happen in
your mind, it's not so easy to break them down into parts. If you can't
observe enough details about a problem, or there aren't any clear internal
boundaries along which you could divide it, top-down will not get you very
far. As an "analytical person" you're probably quite good at breaking things
down, so chances are you'll be tempted to make up boundaries, even you have no
idea whether they are useful. Of course, breaking something down the wrong way
means that anything close to your imaginary boundaries is not fully reflected
in the model you create.

How do you work around that? Well, unfortunately you'll have to let go of the
idea of using a top-down approach here. Fortunately, your brain is extremely
good at bottom-up processing. For instance, it manages to build a cohesive
image out of signals from hundreds of millions of individual visual receptors,
and the result typically makes perfect sense to you and you don't even have to
think about it. That's what bottom-up processing does, and your brain does a
ton of it without you ever noticing.

That's also the downside of bottom-up processing: you get no insight into how
exactly it does what it does. Some people just take everything it comes up
with for granted, but I'm guessing you're here because you don't want to go
down that road. Well, obviously you can't second-guess every little thing that
your comes up in your mind, but let's see if we can't figure out a few guiding
principles and a way to influence your mind's bottom-up processing.

> **TODO** bottom-up: related small things, skill-building

> **TODO** using simulation for building blocks

> **TODO** imagination and how it works (what words do we use to
> conceptualize it: pretend, recall, what if etc.)

## Teaching yourself to trick yourself for fun and profit

Further up, I showed you a number of ways to "attack" mental patterns more
directly. If you want to do the same things to bigger patterns, you either
need a *lot* of practice beforehand, or take a detour of sorts: get your mind
accustomed to the idea of accepting ideas just because you want to (which, of
course, implies that you keep full control over what you accept and what you
don't accept!).

The more you get used to that, the easier it becomes to use more symbolic
approaches to changing patterns in your mind... and what are all the magical
methods and acronyms I listed above if not symbolism?

> **TODO** next up: low-intensity, mid term approach

> **TODO** integrate FPC structure

> **TODO** equanimity, GABA? vipassana approach/exercises

> **TODO** focus more on interrupting patterns ("amygdala")?

## **TODO** our own magical state