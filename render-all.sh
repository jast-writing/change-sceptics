#!/bin/sh

while read file title; do
    echo "$file.md -- $title"
    perl render.pl $file.md "$title" > public/$file.html
done < titles