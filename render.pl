#!/usr/bin/perl

use Text::Markdown;
local $/;
open my $fh, '<', $ARGV[0] or die "Error reading content: $!";

print <<PRE;
<!DOCTYPE html>
<html><head>
<title>$ARGV[1] (by Jan Krüger)</title>
<link rel="stylesheet" href="main.css">
<meta charset="UTF-8">
<!-- yay, proprietary fake headers -->
<meta name="viewport" content="width=device-width">
</head><body>
PRE

print Text::Markdown::markdown(<$fh>);
print <<'POST';
<script>
var l = document.querySelectorAll('blockquote,h2');
for (var i = 0; i < l.length; i++) {
  if (!l[i].textContent.match(/^\s*TODO/)) continue;
  l[i].setAttribute('class', 'todo');
}
</script>
</body></html>
POST
